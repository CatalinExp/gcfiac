# Already created an App
# resource "google_app_engine_application" "app" {
#   project     = var.project
#   location_id = "europe-west3"
# }

resource "google_project_service" "cloudbuild" {
  project = var.project
  service = "cloudbuild.googleapis.com"

  disable_dependent_services = true
  disable_on_destroy         = false
}

resource "google_project_service" "cloudfunctions" {
  project = var.project
  service = "cloudfunctions.googleapis.com"

  disable_dependent_services = true
  disable_on_destroy         = false
}

resource "google_project_service" "cloudscheduler" {
  project = var.project
  service = "cloudscheduler.googleapis.com"

  disable_dependent_services = true
  disable_on_destroy         = false
}

resource "google_project_service" "vpcaccess" {
  project = var.project
  service = "vpcaccess.googleapis.com"

  disable_dependent_services = true
  disable_on_destroy         = false
}

resource "google_project_service" "pubsub" {
  project = var.project
  service = "pubsub.googleapis.com"

  disable_dependent_services = true
  disable_on_destroy         = false
}

resource "google_pubsub_topic" "sync_suppliers_topic" {
  project = var.project
  name    = "sync-suppliers-${var.env}-topic"

  depends_on = [
    google_project_service.pubsub,
  ]
}

resource "google_compute_network" "net" {
  name                    = "func-${var.env}-vpc-network"
  auto_create_subnetworks = false
}

# resource "google_compute_subnetwork" "subnet" {
#   name          = "func-${var.env}-subnetwork"
#   network       = google_compute_network.net.id
#   ip_cidr_range = "10.8.0.0/28"
#   region        = var.region
# }

resource "google_vpc_access_connector" "connector" {
  name          = "func-${var.env}-vpcconn"
  network       = google_compute_network.net.name
  region        = var.region
  ip_cidr_range = "10.8.0.0/28"

  depends_on = [
    google_project_service.vpcaccess,
  ]
}

resource "google_compute_router" "router" {
  name    = "func-${var.env}-router"
  region  = var.region
  network = google_compute_network.net.id
}

resource "google_compute_address" "address" {
  count  = 2
  name   = "nat-manual-func-ip-${var.env}-${count.index}"
  region = var.region
}

resource "google_compute_router_nat" "nat_manual" {
  name   = "func-${var.env}-router-nat"
  router = google_compute_router.router.name
  region = google_compute_router.router.region

  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = google_compute_address.address.*.self_link
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}

resource "google_storage_bucket" "functions_bucket" {
  project = var.project
  name    = "${var.project}-functions"
}

resource "google_storage_bucket_object" "archive" {
  name   = "sync-suppliers.zip"
  bucket = google_storage_bucket.functions_bucket.name
  source = "${path.module}/src/sync-suppliers.zip"
}

# Create Cloud Function
resource "google_cloudfunctions_function" "sync_suppliers_http_func" {
  project     = var.project
  region      = var.region
  name        = "sync-suppliers-http-${var.env}"
  runtime     = "nodejs14"
  entry_point = "syncHttp"

  source_archive_bucket = google_storage_bucket.functions_bucket.name
  source_archive_object = google_storage_bucket_object.archive.name

  available_memory_mb = 512
  trigger_http        = true

  ingress_settings              = "ALLOW_ALL"
  vpc_connector_egress_settings = "ALL_TRAFFIC"
  vpc_connector                 = google_vpc_access_connector.connector.name

  depends_on = [
    google_project_service.cloudbuild,
    google_project_service.cloudfunctions,
  ]
}

# Create IAM entry so all users can invoke the sync suppliers http function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = var.project
  region         = var.region
  cloud_function = google_cloudfunctions_function.sync_suppliers_http_func.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}

resource "google_cloudfunctions_function" "sync_suppliers_pubsub_func" {
  project     = var.project
  region      = var.region
  name        = "sync-suppliers-pubsub-${var.env}"
  runtime     = "nodejs14"
  entry_point = "syncPubSub"

  source_archive_bucket = google_storage_bucket.functions_bucket.name
  source_archive_object = google_storage_bucket_object.archive.name

  available_memory_mb = 512
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = google_pubsub_topic.sync_suppliers_topic.name
  }

  ingress_settings              = "ALLOW_ALL"
  vpc_connector_egress_settings = "ALL_TRAFFIC"
  vpc_connector                 = google_vpc_access_connector.connector.name

  depends_on = [
    google_project_service.cloudbuild,
    google_project_service.cloudfunctions,
  ]
}

resource "google_cloud_scheduler_job" "sync_suppliers_job" {
  project  = var.project
  region   = "europe-west3"
  name     = "sync-suppliers-${var.env}-job"
  schedule = "every 6 hours"

  pubsub_target {
    topic_name = google_pubsub_topic.sync_suppliers_topic.id
    data       = base64encode("Sync Suppliers")
  }

  depends_on = [
    # google_app_engine_application.app,
    google_project_service.cloudscheduler,
  ]
}
