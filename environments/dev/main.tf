locals {
  env = "dev"
}

provider "google" {
  project = var.project
  region  = var.region
}

module "functions" {
  source  = "../../modules/functions"
  env     = local.env
  project = var.project
  region  = var.region
}
