terraform {
  backend "gcs" {
    bucket = "doc-20210614084828-tfstate"
    prefix = "env/stage"
  }
}
