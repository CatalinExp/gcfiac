locals {
  env = "stage"
}

provider "google" {
  project = var.project
  region  = var.region
}
